package com.edutec.mypostrs.post.model;

import java.util.Date;

public class Post {
    private int idPost;
    private int idUsuario;
    private String texto;
    private String imgPost;
    private Date fechaPost;

    public Post() {
    }

    public Post(int idPost, int idUsuario, String texto, String imgPost, Date fechaPost) {
        this.idPost = idPost;
        this.idUsuario = idUsuario;
        this.texto = texto;
        this.imgPost = imgPost;
        this.fechaPost = fechaPost;
    }

    public int getIdPost() {
        return idPost;
    }

    public void setIdPost(int idPost) {
        this.idPost = idPost;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getImgPost() {
        return imgPost;
    }

    public void setImgPost(String imgPost) {
        this.imgPost = imgPost;
    }

    public Date getFechaPost() {
        return fechaPost;
    }

    public void setFechaPost(Date fechaPost) {
        this.fechaPost = fechaPost;
    }
}
