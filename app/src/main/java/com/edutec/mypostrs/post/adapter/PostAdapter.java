package com.edutec.mypostrs.post.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.edutec.mypostrs.R;
import com.edutec.mypostrs.post.model.Postt;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.MyViewHolder> {
    private FirebaseFirestore db;
    private LayoutInflater inflater;
    private Context context;
    private ArrayList<Postt> posts;

    public PostAdapter(Context context, ArrayList<Postt> posts) {
        db = FirebaseFirestore.getInstance();
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.posts = posts;
    }
    /*
    * Donde inflo la vista del item en el recyclerview.*/
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_post, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }
    /* Recibe para interactuar con nuestro item actual. Con el item seleccionado. */
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // Llenar la vista.
        Postt current = posts.get(position);
        holder.txtUsername.setText(current.getUsername());
        holder.txtTextPost.setText((current.getTexto()));

        holder.cardItem.setOnClickListener(e -> {
            db.collection("posts")
                    .document(current.getId())
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            DocumentSnapshot document = task.getResult();
                            Toast.makeText(context, document.getData().get("username").toString(), Toast.LENGTH_SHORT).show();
                        }
                    });
        });
        //
        /*Postt current = posts.get(position); // Ya tengo cargado mi POST.
        holder.txtUsername.setText(current.getUsername());
        holder.txtTextPost.setText(current.getTexto());
        holder.cardItem.setOnClickListener(e -> {
            Toast.makeText(context, current.getTexto(), Toast.LENGTH_SHORT).show();
        });*/
    }
    /* Obtiene el tamaño del arraylist guardado en el recyclerview. */
    @Override
    public int getItemCount() {
        return posts.size(); // El recyclerview cuantos datos tiene que cargar.
    }

    // Nos servira para conectarnos con el ITEM.
    class MyViewHolder extends RecyclerView.ViewHolder {
        // Para conectarnos a nuestr vista. Nombre de usuario y texto.
        TextView txtUsername;
        TextView txtTextPost;
        CardView cardItem;
        public MyViewHolder(View itemView) {
            super(itemView);
            // Nos conectamos con cada item de nuestra vista
            txtUsername = itemView.findViewById(R.id.txtUsername);
            txtTextPost = itemView.findViewById(R.id.txtTextPost);
            cardItem = itemView.findViewById(R.id.cardItem);
        }
    }
}
