package com.edutec.mypostrs.post.model;

public class Postt {
    private String id, texto, username;

    public Postt() {
    }

    public Postt(String id, String texto, String username) {
        this.id = id;
        this.texto = texto;
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
