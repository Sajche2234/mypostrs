package com.edutec.mypostrs.post.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.edutec.mypostrs.R;
import com.edutec.mypostrs.post.adapter.PostAdapter;
import com.edutec.mypostrs.post.model.Postt;
import com.edutec.mypostrs.user.activity.SignInActivity;
import com.edutec.mypostrs.user.activity.UsersActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;

public class MypostActivity extends AppCompatActivity {
    RecyclerView recyclerMyPosts;
    private ArrayList<Postt> posts;
    private PostAdapter adapter;
    private FirebaseFirestore db;
    Button btnOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mypost);
        connect();
        // Cargar datos.
        posts = new ArrayList<>();
        db = FirebaseFirestore.getInstance();
        db.collection("posts")
                .whereEqualTo("username",  "Roberto")
                .get()
                .addOnCompleteListener(onComplete -> {
                    if(onComplete.isSuccessful()){
                        for (QueryDocumentSnapshot document : onComplete.getResult()){
                            Postt post = new Postt();
                            post.setId(document.getId());
                            post.setUsername(document.getData().get("username").toString());
                            post.setTexto(document.getData().get("texto").toString());
                            posts.add(post);
                        }
                        adapter = new PostAdapter(this, posts);
                        recyclerMyPosts.setAdapter(adapter);
                    }
                });
        // Cerrar Sesión.
        btnOut.setOnClickListener(e -> {
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(this, SignInActivity.class));
            finish();
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, UsersActivity.class));
        finish();
    }

    private void connect(){
        recyclerMyPosts = findViewById(R.id.recyclerMyPosts);
        recyclerMyPosts.setLayoutManager(new LinearLayoutManager(this)); // Que tipo de vista queremos.
        btnOut = findViewById(R.id.btnOut);
    }
}
