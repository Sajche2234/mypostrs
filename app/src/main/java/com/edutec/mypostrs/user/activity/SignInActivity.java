package com.edutec.mypostrs.user.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.edutec.mypostrs.R;
import com.edutec.mypostrs.post.activity.PostActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SignInActivity extends AppCompatActivity {
    private FirebaseAuth auth;
    private EditText edtEmail, edtPassword;
    private Button btnSignIn;
    private TextView txtRegistro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        connect();
        auth = FirebaseAuth.getInstance();
        btnSignIn.setOnClickListener(e -> {
            String email, password;
            email = edtEmail.getText().toString();
            password = edtPassword.getText().toString();
            signInWithEmailAndPassword(email, password);
        });
        txtRegistro.setOnClickListener(e-> {
            startActivity(new Intent(this, SignUpActivity.class));
            finish();
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = auth.getCurrentUser();

        if(currentUser != null){
            startActivity(new Intent(this, UsersActivity.class));
            finish();
        }
    }

    private void signInWithEmailAndPassword(String email,  String password){
        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, onComplete -> {
                    FirebaseUser currentUser;
                    if(onComplete.isSuccessful()){
                        currentUser = auth.getCurrentUser();
                        startActivity(new Intent(this, UsersActivity.class));
                        finish();
                    } else {
                        currentUser = null;
                        Toast.makeText(this, "Debe iniciar sesión.", Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private void connect(){
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        btnSignIn = findViewById(R.id.btnSignIn);
        txtRegistro = findViewById(R.id.txtRegistro);
    }
}
