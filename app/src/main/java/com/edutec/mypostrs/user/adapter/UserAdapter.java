package com.edutec.mypostrs.user.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.edutec.mypostrs.R;
import com.edutec.mypostrs.user.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyViewHolder> {
    private FirebaseFirestore db;
    private LayoutInflater inflater;
    private Context context;
    private ArrayList<User> users;

    public UserAdapter(Context context, ArrayList<User> users) {
        db = FirebaseFirestore.getInstance();
        this.context = context;
        this.users = users;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_user, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        User current = users.get(position);
        holder.txtUsername.setText(current.getUsername());
        holder.txtName.setText(current.getName());
        holder.txtEmail.setText(current.getEmail());
        holder.cardItem.setOnClickListener(e -> {
            db.collection("users")
                    .document(current.getId())
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            DocumentSnapshot document = task.getResult();
                            Toast.makeText(context, document.getData().get("username").toString(), Toast.LENGTH_SHORT).show();
                        }
                    });
        });
        // Eliminar registro
        holder.cardItem.setOnLongClickListener(e -> {
            db.collection("users")
                    .document(current.getId())
                    .delete()
                    .addOnSuccessListener(onSuccess -> {
                        Toast.makeText(context, "Eliminado exitosamente", Toast.LENGTH_SHORT).show();
                        users.remove(current);
                        notifyDataSetChanged(); // Refrezcando la vista, despues de haber eliminado.
                    })
                    .addOnFailureListener(onFailure -> {
                        Toast.makeText(context, "Eliminacion fallida", Toast.LENGTH_SHORT).show();
                    });
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtUsername,txtEmail,txtName;
        CardView cardItem;
        public MyViewHolder(View itemView){
            super(itemView);
            txtUsername = itemView.findViewById(R.id.txtUsername);
            txtEmail = itemView.findViewById(R.id.txtEmail);
            txtName = itemView.findViewById(R.id.txtName);
            cardItem = itemView.findViewById(R.id.cardItem);
        }
    }

}
