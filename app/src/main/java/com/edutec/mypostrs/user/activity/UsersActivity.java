package com.edutec.mypostrs.user.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.edutec.mypostrs.R;
import com.edutec.mypostrs.post.activity.PostActivity;
import com.edutec.mypostrs.user.adapter.UserAdapter;
import com.edutec.mypostrs.user.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;

public class UsersActivity extends AppCompatActivity {
    RecyclerView recyclerUsers;
    private UserAdapter adapter;
    private ArrayList<User> users;
    private FirebaseFirestore db;
    private FirebaseAuth auth;
    private FirebaseUser fUser;
    private Button btnOut, btnPosts;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        connect();
        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        users = new ArrayList<>();
        db.collection("users")
                .get()
                .addOnCompleteListener(onComplete -> {
                    if(onComplete.isSuccessful()){
                        for(QueryDocumentSnapshot document : onComplete.getResult()) {
                            User user = new User();
                            user.setId(document.getId());
                            user.setName(document.getData().get("name").toString());
                            user.setEmail(document.getData().get("email").toString());
                            user.setUsername(document.getData().get("username").toString());
                            users.add(user);
                        }
                        adapter = new UserAdapter(this, users);
                        recyclerUsers.setAdapter(adapter);
                    } else {
                        Toast.makeText(this, "Error al leer los documentos", Toast.LENGTH_SHORT).show();
                    }
                });
        btnOut.setOnClickListener(e -> {
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(this, SignInActivity.class));
            finish();
        });
        btnPosts.setOnClickListener(e -> {
            startActivity(new Intent(this, PostActivity.class));
            finish();
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        fUser = auth.getCurrentUser();
        /*db.collection("users")
                .document(fUser.getUid())
                .get()*/
    }

    private void connect(){
        recyclerUsers = findViewById(R.id.recyclerUsers);
        recyclerUsers.setLayoutManager(new LinearLayoutManager(this));
        btnOut = findViewById(R.id.btnOut);
        btnPosts = findViewById(R.id.btnPosts);
    }
}
