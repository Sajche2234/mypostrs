package com.edutec.mypostrs.user.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.edutec.mypostrs.R;
import com.edutec.mypostrs.post.activity.PostActivity;
import com.edutec.mypostrs.post.adapter.PostAdapter;
import com.edutec.mypostrs.user.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity {
    private FirebaseAuth auth;
    private EditText edtEmail, edtPassword, edtName, edtUsername;
    private Button btnSignUp;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        connect();
        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        btnSignUp.setOnClickListener(e -> {
            String email, password;
            email = edtEmail.getText().toString();
            password = edtPassword.getText().toString();
            signUpWithEmailAndPassword(email, password);
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = auth.getCurrentUser();
        if(currentUser != null){
            startActivity(new Intent(this, PostAdapter.class));
            finish();
        }
    }

    private void signUpWithEmailAndPassword(String email, String password) {
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, onComplete -> {
                    FirebaseUser currentUser;
                    if(onComplete.isSuccessful()) {
                        currentUser = auth.getCurrentUser();
                        createAccount(currentUser);
                        startActivity(new Intent(this, PostActivity.class));
                        finish();
                    } else {
                        currentUser = null;
                        Toast.makeText(this, "Error al crear la cuenta", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void createAccount(FirebaseUser firebaseUser){
        if(firebaseUser != null){
            Map<String, Object> data = new HashMap<>();
            User user = new User();
            user.setName(edtName.getText().toString());
            user.setUsername(edtUsername.getText().toString());
            user.setEmail(firebaseUser.getEmail());
            user.setId(firebaseUser.getUid());
            // Construir la DATA.
            data.put("name", user.getName());
            data.put("username", user.getUsername());
            data.put("email", user.getEmail());
            data.put("id", user.getId());

            Timestamp timestamp = Timestamp.now();
            Long tmpId = timestamp.getSeconds();
            // Ruta a la que vamos a llegar. Add a new document with a generated ID -- tmpId.toString() ---  user.getId()
            db.collection("users")
                    .document(tmpId.toString())
                    .set(data)
                    .addOnSuccessListener(aVoid -> {
                        Toast.makeText(this, "Exitoso", Toast.LENGTH_SHORT).show();
                    })
                    .addOnFailureListener(onFailure -> {
                        Toast.makeText(this, "Error al almacenar", Toast.LENGTH_SHORT).show();
                    });
        } else {
            Toast.makeText(this, "Error. El usuario no fue ingresado", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, SignInActivity.class));
        finish();
    }

    private void connect(){
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        btnSignUp = findViewById(R.id.btnSignUp);
        edtName = findViewById(R.id.edtName);
        edtUsername = findViewById(R.id.edtUsername);
    }
}
